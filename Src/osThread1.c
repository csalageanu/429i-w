/* Includes ------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f429i_discovery.h"

#include "hw_init.h"

#include "stm32f4xx_hal_msp.h"

#include "osThread1.h"

#ifdef USE_FREE_RTOS
#include "Tst_CntGUI.h"
#endif

/* Private typedef -----------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#ifdef USE_FREE_RTOS
/* FreeRTOS thread for test */
osThreadId Thread1Handle;
#endif

/* Private function prototypes -----------------------------------------------*/
/*----------------------------------------------------------------------------*/
#ifdef USE_FREE_RTOS
static void OS_Thread1(void const *argument);
#endif

/* Exported functions --------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
void Thread1_Init(void)
{
#ifdef USE_FREE_RTOS
  /* Thread 1 definition */
  osThreadDef(LED3, OS_Thread1, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);

  /* Start thread 1 */
  Thread1Handle = osThreadCreate (osThread(LED3), NULL);
  /***********************************************************/
#endif
}

/* Private functions ---------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#ifdef USE_FREE_RTOS
static void OS_Thread1(void const *argument)
{
  (void) argument;

  TstCntGui_Init();

  for(;;)
  {
	TstCntGui_Run();
	osDelay(1000);
  }
}
#endif
