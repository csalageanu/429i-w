/* Includes ------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f429i_discovery.h"

#include "hw_init.h"

#include "stm32f4xx_hal_msp.h"

/* Private typedef -----------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
TIM_HandleTypeDef TimHandle;

/* Private function prototypes -----------------------------------------------*/
/*----------------------------------------------------------------------------*/
static void HAL_MainAppTmr_Init(void);


/* Exported functions --------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

/**
  * @brief  Initialization function for this module
  * @param  None
  * @retval None
  */
void HAL_Msp_Init(void)
{
	HAL_MainAppTmr_Init();
}

/* Private functions ---------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/* **********************
 * *** Timers section ***
 * **********************/

/**
  * @brief  Initialize Main Application Timer
  * @param  None
  * @retval None
  */
static void HAL_MainAppTmr_Init(void)
{
	uint32_t uwPrescalerValue = 0;

	/* Compute the prescaler value to have TIM3 counter clock equal to 10 KHz */
	uwPrescalerValue = (uint32_t) ((SystemCoreClock /2) / 10000) - 1;

	/* Set TIMx instance */
	TimHandle.Instance = TIM3;

	/* Initialize TIM3 peripheral as follows:
		+ Period = 500 - 1
		+ Prescaler = ((SystemCoreClock/2)/10000) - 1
		+ ClockDivision = 0
		+ Counter direction = Up
	*/
	TimHandle.Init.Period = 500 - 1;
	TimHandle.Init.Prescaler = uwPrescalerValue;
	TimHandle.Init.ClockDivision = 0;
	TimHandle.Init.CounterMode = TIM_COUNTERMODE_UP;

	if(HAL_TIM_Base_Init(&TimHandle) != HAL_OK)
	{
		while(1)
		{
		}
	}

	/* Start the TIM Base generation in interrupt mode */
	/* Start Channel1 */
	if(HAL_TIM_Base_Start_IT(&TimHandle) != HAL_OK)
	{
		while(1)
		{
		}
	}
}

/**
  * @brief TIM MSP Initialization (called from HAL_TIM_Base_Init() - library function)
  *        This function configures the hardware resources used in This application:
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration
  * @param htim: TIM handle pointer
  * @retval None
  */
void HAL_TIM_Base_MspInit(TIM_HandleTypeDef *htim)
{
  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* TIMx Peripheral clock enable */
  __HAL_RCC_TIM3_CLK_ENABLE();

  /*##-2- Configure the NVIC for TIMx ########################################*/
  /* Set the TIMx priority */
  HAL_NVIC_SetPriority(TIM3_IRQn, 0, 1);

  /* Enable the TIMx global Interrupt */
  HAL_NVIC_EnableIRQ(TIM3_IRQn);
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @param  htim: TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if (htim == &TimHandle)
	{
		HwIni_Main();
	}
}

