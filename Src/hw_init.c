/* Includes ------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "GUI.h"

#include "stm32f429i_discovery.h"
#include "stm32f429i_discovery_ts.h"
#include "stm32f429i_discovery_sdram.h"
#include "..\Components\ili9341\ili9341.h"

#include "hw_init.h"

/* Private typedef -----------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
static uint8_t GUI_Initialized;

/* Private function prototypes -----------------------------------------------*/
/*----------------------------------------------------------------------------*/
static void BSP_Pointer_Update(void);

/* Exported functions --------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/**
  * @brief  Initializes the STM32F429I-DISCO's LCD and LEDs resources.
  * @param  None
  * @retval None
  */
void HwIni_Init(void)
{
	GUI_Initialized = 0;

	/* Initialize STM32F429I-DISCO's LEDs */
	BSP_LED_Init(LED3);
	BSP_LED_Init(LED4);

	/* Initializes the SDRAM device */
	BSP_SDRAM_Init();

	/* Initialize the Touch screen */
	BSP_TS_Init(240, 320);

	/* Enable the CRC Module */
	__HAL_RCC_CRC_CLK_ENABLE();
}

/**
  * @brief  HwIni_Main
  * @param  None
  * @retval None
  */
void HwIni_Main(void)
{
	BSP_LED_Toggle(LED3);

	/* Capture input event and update cursor */
	if(GUI_Initialized == 1)
	{
		BSP_Pointer_Update();
	}
}

/**
  * @brief  HwIni_ev_GuiInitialized
  * @param  None
  * @retval None
  */
void HwIni_ev_GuiInitialized(void)
{
	GUI_Initialized = 1;
}

/* Private functions ---------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/**
  * @brief  Provide the GUI with current state of the touch screen
  * @param  None
  * @retval None
  */
static void BSP_Pointer_Update(void)
{
  GUI_PID_STATE TS_State;
  static TS_StateTypeDef prev_state;
  TS_StateTypeDef  ts;
  uint16_t xDiff, yDiff;

  BSP_TS_GetState(&ts);

  TS_State.Pressed = ts.TouchDetected;

  xDiff = (prev_state.X > ts.X) ? (prev_state.X - ts.X) : (ts.X - prev_state.X);
  yDiff = (prev_state.Y > ts.Y) ? (prev_state.Y - ts.Y) : (ts.Y - prev_state.Y);

  if(ts.TouchDetected)
  {
    if((prev_state.TouchDetected != ts.TouchDetected )||
       (xDiff > 3 )||
         (yDiff > 3))
    {
      prev_state = ts;

      TS_State.Layer = 0;
      TS_State.x = ts.X;
      TS_State.y = ts.Y;

      GUI_TOUCH_StoreStateEx(&TS_State);
    }
  }
}
