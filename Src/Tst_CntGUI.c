/* Includes ------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f429i_discovery.h"
#include "WM.h"
#include "GUI.h"

#include "hw_init.h"
#include "stm32f4xx_hal_msp.h"

#include "Tst_CntGUI.h"

/* Private typedef -----------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/* Counter, display value on LCD */
static uint16_t tsk_cnt;

/* Private function prototypes -----------------------------------------------*/
/*----------------------------------------------------------------------------*/
static void UserTest(uint16_t * const cnt);

/* Exported functions --------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/**
  * @brief  TstCntGui_Init
  * @param  None
  * @retval None
  */
void TstCntGui_Init(void)
{
	tsk_cnt = 0;

	/* Init the STemWin GUI Library */
	GUI_Init();
	HwIni_ev_GuiInitialized();

	/* Activate the use of memory device feature */
	WM_SetCreateFlags(WM_CF_MEMDEV);

	GUI_Clear();
	GUI_SetFont(&GUI_Font20_1);
}

/**
  * @brief  TstCntGui_Run
  * @param  None
  * @retval None
  */
void TstCntGui_Run(void)
{
	UserTest(&tsk_cnt);
}

/* Private functions ---------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/**
  * @brief  Function to display a counter on screen and to toggle a led
  * @param  Pointer to an uint16_t, used as counter
  * @retval None
  */
static void UserTest(uint16_t * const cnt)
{
	static char str[6] = {0};
	/* Above var has to be static else when USE_FREE_RTOS is defined
	 * the test will not run: counter is incremented only once and
	 * LED remains ON. I don't know why. */

	BSP_LED_Toggle(LED4);
	(*cnt)++;
	snprintf(str, sizeof(str), "%i", *cnt);
	GUI_Clear();
	GUI_DispStringAt(str, (LCD_GetXSize()-100)/2, (LCD_GetYSize()-20)/2);
}
