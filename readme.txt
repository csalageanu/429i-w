Project running on STM32F429i Discovery and print a counter on display.


HOW TO RE-BUILD:

 - Open System Workbench for STM32. Any workspace shoul be ok.

 - Import the project as following
     File >> Import...
     Click on "Browse" and navigate to  
     "your_location"\SW4STM32\White

     The projects list is refreshed and should display System Workbench project ("..\SW4STM32\White"), select it.
     Ensure the option "Copy projects into workspace" is uncheck (in fact none of the checkbox should be checked)
     Click on "Finish" button.

 - Build the project (you can Clean first because I did integrate also my Debug direcotry)
   During the build process a window pop-up with a message "Auto-ignore derived resources". Ignore message, will disapear at the end of the build.
   Refresh (F5) the project.

 - Check if the debugger configuration is present. If it is present try to debug. If not create
   a new one, following the steps described at below link - at the end. 
"Importing a STCubeMX generated project", at this link: [http://www.openstm32.org/Importing+a+STCubeMX+generated+project?structure=Documentation]