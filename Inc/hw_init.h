#ifndef __BSP_USR_H
#define __BSP_USR_H

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

 /* Exported functions prototypes ---------------------------------------------*/
 /*----------------------------------------------------------------------------*/
extern void HwIni_Init(void);
extern void HwIni_Main(void);
extern void HwIni_ev_GuiInitialized(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __BSP_USR_H */
