#ifndef __STM32F4xx_HAL_MSP_H
#define __STM32F4xx_HAL_MSP_H

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

 /* Exported functions prototypes ---------------------------------------------*/
 /*----------------------------------------------------------------------------*/
extern void HAL_Msp_Init(void);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __STM32F4xx_HAL_MSP_H */
