#ifndef __OS_THREAD_1_H
#define __OS_THREAD_1_H

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

 /* Includes ------------------------------------------------------------------*/
 /*----------------------------------------------------------------------------*/
#include "cmsis_os.h"

/* Exported define ------------------------------------------------------------*/
/*-----------------------------------------------------------------------------*/
 /* Uncomment below definition if FreeRTOS need to be used to increment the
  * counter on LCD and to toggle the LED. If not this will be done without
  * RTOS */
#define USE_FREE_RTOS

 /* Exported functions prototypes ---------------------------------------------*/
 /*----------------------------------------------------------------------------*/
extern void Thread1_Init(void);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __OS_THREAD_1 */
