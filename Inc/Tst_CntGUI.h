#ifndef __TST_CNT_GUI_H
#define __TST_CNT_GUI_H

#ifdef __cplusplus
 extern "C" {
#endif /* __cplusplus */

 /* Exported functions prototypes ---------------------------------------------*/
 /*----------------------------------------------------------------------------*/
extern void TstCntGui_Init(void);
extern void TstCntGui_Run(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __TST_CNT_GUI_H */